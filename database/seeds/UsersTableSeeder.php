<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    User::create([
	        'name' => 'Jhonny Carpenito',
	        'email' => 'jhonny@example.com',
	        'password' => bcrypt('123123'),
	        'role' => 'admin'
	    ]);	

	    User::create([
	        'name' => 'Paciente 1',
	        'email' => 'patient@example.com',
	        'password' => bcrypt('123123'),
	        'role' => 'patient'
	    ]);	

	    User::create([
	        'name' => 'Doctor 1',
	        'email' => 'doctor@example.com',
	        'password' => bcrypt('123123'),
	        'role' => 'doctor'
	    ]);	

       factory(User::class, 50)->states('patient')->create(); 
    }
}
