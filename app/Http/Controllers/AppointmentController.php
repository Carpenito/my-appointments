<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specialty;
use App\Appointment;
use App\CancelledAppointment;

use Carbon\Carbon;
use App\Intefaces\ScheduleServiceInterface;

use Validator;


class AppointmentController extends Controller
{
    public function index(){

		$role = auth()->user()->role;
		
		// patient
		// doctor
		if($role == 'admin'){
			$pendingAppointments = Appointment::where('status', 'Reserveda')
				->paginate(10);
			$confirmedAppointments = Appointment::where('status', 'Confirmada')
				->paginate(10);
			$oldAppointments = Appointment::whereIn('status', ['Atendida','Cancelada'])
				->paginate(10);			
		}elseif($role == 'doctor'){
			$pendingAppointments = Appointment::where('status', 'Reserveda')
				->where('doctor_id', auth()->id())
				->paginate(10);
			$confirmedAppointments = Appointment::where('status', 'Confirmada')
				->where('doctor_id', auth()->id())	
				->paginate(10);
			$oldAppointments = Appointment::whereIn('status', ['Atendida','Cancelada'])
				->where('doctor_id', auth()->id())	
				->paginate(10);	
		}elseif($role == 'patient'){
			$pendingAppointments = Appointment::where('status', 'Reserveda')
				->where('patient_id', auth()->id())
				->paginate(10);
			$confirmedAppointments = Appointment::where('status', 'Confirmada')
				->where('patient_id', auth()->id())	
				->paginate(10);
			$oldAppointments = Appointment::whereIn('status', ['Atendida','Cancelada'])
				->where('patient_id', auth()->id())	
				->paginate(10);			
		}




		// admin-> all


		return view('appointments.index', compact('pendingAppointments','confirmedAppointments','oldAppointments','role'));
	}

	public function show(Appointment $appointment)
	{
		$role = auth()->user()->role;
		return view('appointments.show', compact('appointment','role'));
	}
	
	public function create(ScheduleServiceInterface $scheduleService)
    {
    	$specialties = Specialty::all();

    	$specialtyId = old('specialty_id');
    	if($specialtyId){
    		$specialty = Specialty::find($specialtyId);
    		$doctors = $specialty->users;
    	}else {
    		$doctors = collect();
    	}	


    	$date = old('scheduled_date');
    	$doctorId = old('doctor_id');
    	if($date && $doctorId){
    		$intervals = $scheduleService->getAvalaibleInterval($date, $doctorId);;
    	} else {
    		$intervals = null;
    	}

    	return view('appointments.create', compact('specialties','doctors','intervals'));
    }

    public function store(Request $request, ScheduleServiceInterface $scheduleService)
    {
    	$rules = [
    		'description' => 'required',
	    	'specialty_id' => 'exists:specialties,id',
	    	'doctor_id' => 'exists:users,id',
	    	'scheduled_time' => 'required',
    	];
    	$messages = [
    		'scheduled_time.required' => 'Por favor seleccione una hora valida para su cita.'
    	];

    	$validator = Validator::make($request->all(), $rules, $messages);

        $validator->after( function ($validator) use ($request, $scheduleService){
            $date = $request->input('schedule_date');
            $doctorId = $request->input('doctor_id');
            $scheduled_time = $request->input('scheduled_time');
            if($date && $doctorId && $schedule_time){
                $start = new Carbon($scheduled_time);
            } else {
                return;
            }

            if(!$scheduleService->isAvalaibleInterval($date,$doctorId,$start)){
                $validator->errors()
                    ->add('available_time','La hora seleccionada ya se encuentra reservada por otro paciente.');
            }
        });


        if ($validator->fails()){
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

    	$data = $request->only([
	    	'description',
	    	'specialty_id',
	    	'doctor_id',
	    	'scheduled_date',
	    	'scheduled_time',
	    	'type',
    	]);
    	$data['patient_id'] = auth()->id();
    	$carbonTime = Carbon::createFromFormat('g:i A', $data['scheduled_time']);
    	$data['scheduled_time'] = $carbonTime->format('H:i:s');
    	Appointment::create($data);

    	$notification = 'La cita se ha registrado correctamente!';
    	return back()->with(compact('notification'));

    	// return redirect('/appointments');

    }

	public function showCancelForm(Appointment $appointment){

		if($appointment->status == 'Confirmada'){
			$role = auth()->user()->role;
			return view('appointmets.cancel', compact('appointment','role'));
		}

		return redirect('/appointments');
	}


	public function postCancel(Appointment $appointment, Request $request){
		if($request->has('justification')) {
			$cancellation = new CancelledAppointment();
			$cancellation->justification = $request->input('justification');
			$cancellation->canelled_by = auth()->id();

			$appointment->cancellation()->save($cancellation);
		}
		
		$appointment->status = 'Cancelada';
		$appointment->save();

		$notification = 'La cita se ha cancelado correctamente';
		return redirect('/appointments')->with(compact('notificacion'));
	}

	public function postConfirm(Appointment $appointment){
		
		$appointment->status = 'Confirmada';
		$appointment->save();

		$notification = 'La cita se ha confirmado correctamente';
		return redirect('/appointments')->with(compact('notificacion'));
	}
}
